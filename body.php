<?php
echo "<div id=\"img-strip\">
		        <a href=\"/index.html\"><img src=\"/media/trailer.jpg\" width=\"750\" height=\"150\" alt=\"trailer\"></a>
			</div>
			<div id=\"menubar\">
				<a href=\"/index.html\"><h1>Home</h1></a>
				<div class=\"tab\">
					<button class=\"tablinks\" id=\"close\" onclick=\"closeMenu()\"><img src=\"/media/red_x.png\" alt=\"Menü schließen\"width=\"32\" height=\"32\"></button>
					<button class=\"tablinks\" onclick=\"openMenu(event,'beta')\">Phase 3</button>
					<button class=\"tablinks\" onclick=\"openMenu(event,'stationsverwaltung')\"><del>Stationen</del></button>
					<button class=\"tablinks\" onclick=\"openMenu(event,'produktverwaltung')\"><del>Produkte</del></button>
					<button class=\"tablinks\" onclick=\"openMenu(event,'inventarverwaltung')\"><del>Inventar</del></button>
					
				</div>
				<div id=\"stationsverwaltung\" class=\"tabcontent\">
					<h3>Stationsverwaltung</h3>
					<a href=\"/stationsverwaltung/neue_verkaufsstelle.html\"><span>Verkaufsstelle er&ouml;ffnen</span></a><br>
					<a href=\"/stationsverwaltung/verkaufsstellen_auflisten.html\"><span>Verkaufsstellen auflisten</span></a><br>
					<a href=\"/stationsverwaltung/verkaufsstelle_standort_bestimmen.html\"><span>Verkaufsstellenposition</span></a> <br>
					<a href=\"/stationsverwaltung/verkaufsstelle_suchen.html\"><span>Verkaufsstelle suchen</span></a><br>
					<a href=\"/stationsverwaltung/verkaufsstelle_bearbeiten.html\"><span>Verkaufsstelle bearbeiten</span></a><br>
					<a href=\"/stationsverwaltung/verkaufsstelle_schließen.html\"><span>Verkaufsstelle schließen</span></a><br>
				</div>
				<div id=\"produktverwaltung\" class=\"tabcontent\">
					<h3>Produktverwaltung</h3>
					<a href=\"neues_produkt.html\"><span>Neues Produkt erstellen</span></a><br>
					<a href=\"produkte_auflisten.html\"><span>Produkte auflisten</span></a><br>
					<a href=\"produkt_loeschen.html\"><span>Produkt löschen</span></a><br>
					<a href=\"produkt_aendern.html\"><span>Produkt ändern</span></a>
				</div>
				<div id=\"inventarverwaltung\" class=\"tabcontent\">
					<h3>Inventar- und Lieferverwaltung</h3>
					<a href=\"neue_lieferung.html\"><span>Neue Lieferung erstellen</span></a><br>
					<a href=\"inventar_auflisten.html\"><span>Inventar einer Station auflisten</span></a><br>
					<a href=\"inventar_korrigieren.html\"><span>Inventar einer Station korrigieren</span></a><br>
					<a href=\"bestaende_zu_produkt_auflisten.html\"><span>Bestände eines Produktes auflisten</span></a><br>
				</div>
				<div id=\"beta\" class=\"tabcontent\">
				    <h3>Beta</h3>
				    <a href=\"/localhost:8080/reporting/\" target='_blank' onclick=\"event.target.port=8080\"><span>Reporting</span></a><br>
				    <a href=\"/beta/lieferliste.html\"><span>Lieferliste erstellen</span></a><br>
				    <a href=\"/beta/lieferliste.html\"><span>Lieferung erfassen</span></a><br>
				    <a href=\"/beta/verkauf.html\"><span>Verkauf erfassen</span></a>
                </div>
			</div>";
