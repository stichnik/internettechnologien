<?php


$data = array_merge($_GET, $_POST);
$action = $data['action'];
$type = $data['type'];
$db = new mysqli("spectre","phpuser","07jXg4dX2WsqkDg2", "itt");

switch($action){
    case'POST':
        if($type=='lief')
            lieferungBuchen($data, $db);
        else if($type=='verk')
            verkaufBuchen($data, $db);
        break;

    case'GET':
        if($type=='inv')
            sendLagerbestand($data, $db);
        else if($type=='preis')
            sendPreisliste($data, $db);
        break;
}
function lieferungBuchen($data,$db){
    $produkte = ["milch","emmentaler","gouda","joghurt100","quark","joghurt500","streichkaese","bergkaese"];

    $vkortnr = $data['vkortnr'];

    //Zuerst einen neuen Eintrag in Lieferung erstellen
    $sql1 = 'INSERT INTO lieferung (vkortnr, datum) VALUES (' . $vkortnr . ', CURDATE());';
    $result = $db->query($sql1);
    if($result != 1){
        echo "ERROR TRYING TO INSERT INTO DATABASE";
        return;
    }
    $sql2 = 'SELECT LAST_INSERT_ID();';

    $liefernr =$db->query($sql2)->fetch_assoc();
    $liefernr = $liefernr['LAST_INSERT_ID()'];
    for($i=0;$i<8;){
        $menge = $data[$produkte[$i]];
        $i++;
        if($menge==0)
            continue;
        //Dann jeweils einen Eintrag in lieferungprodukt für jedes Produkt
        $sql3 = 'INSERT INTO lieferungprodukt (liefernr, artikelnr, anzahl) VALUES(' . $liefernr . ', ' . $i . ', ' . $menge . ');';
        $result = $db->query($sql3);
        if($result!=1){
            echo "ERROR TRYING TO ADD LIEFERUNGPRODUKT";
            return;
        }
        //Und update menge in Inventar
        $sql4 = 'UPDATE lagerbestand SET anzahl = anzahl + ' . $menge . ' WHERE vkortnr=' . $vkortnr . ' AND artikelnr=' . $i . ';';
        $result = $db->query($sql4);
        if($result!=1){
            echo "ERROR TRYING TO ADD LIEFERUNGPRODUKT";
            return;
        }


    }
    echo "SUCCESS ADDING LIEFERUNG";
    return;

}
function verkaufBuchen($data,$db){
    $produkte = ["milch","emmentaler","gouda","joghurt100","quark","joghurt500","streichkaese","bergkaese"];
    $preise = getPreisliste($data,$db);
    $vkortnr = $data['vkortnr'];
    $gesamtpreis=0;
    $lagerleer=false;
    $menge = [];
    for($i=0;$i<8;$i++){
        $menge[$i] = $data[$produkte[$i]];
        $preis[$i] = $preise[$i+1]["preis"];
        $gesamtpreis+=$menge[$i]*$preis[$i];
        $sqltest = "SELECT anzahl FROM lagerbestand WHERE vkortnr = " . $vkortnr . " AND artikelnr = " . ($i+1) . ";";
        $result = $db->query($sqltest)->fetch_assoc();
        if($result["anzahl"]<$menge[$i])
            $lagerleer=true;
    }
    if($gesamtpreis==0){
        echo "Fehler: Leere Transaktion";
        return;
    }else if($lagerleer==true){
        echo "Fehler: Nicht genügend Artikel im Lager für Transaktion";
        return;
    }

    //Zuerst einen neuen Eintrag in Verkauf erstellen
    $sql1 = 'INSERT INTO verkauf (vkortnr, datum, warenwert) VALUES (' . $vkortnr . ', CURDATE(), ' . $gesamtpreis . ');';
    $result = $db->query($sql1);
    if($result != 1){
        $error = mysqli_error($db);
        echo "ERROR TRYING TO INSERT INTO DATABASE";
        return;
    }
    //verkfnr holen
    $sql2 = 'SELECT LAST_INSERT_ID();';
    $verkfnr =$db->query($sql2)->fetch_assoc();
    $verkfnr = $verkfnr['LAST_INSERT_ID()'];
    for($i=0;$i<8;$i++){
        if($menge[$i]==0)
            continue;
        //Dann jeweils einen Eintrag in verkaufprodukt für jedes Produkt
        $sql3 = 'INSERT INTO verkaufprodukt (verkfnr, artikelnr, anzahl) VALUES(' . $verkfnr . ', ' . ($i+1) . ', ' . $menge[$i] . ');';
        $result = $db->query($sql3);
        if($result!=1){
            echo "ERROR TRYING TO ADD VERKAUFPRODUKT";
            return;
        }
        //Und update menge in Inventar
        $sql4 = 'UPDATE lagerbestand SET anzahl = anzahl - ' . $menge[$i] . ' WHERE vkortnr=' . $vkortnr . ' AND artikelnr=' . ($i+1) . ';';
        $result = $db->query($sql4);
        if($result!=1){
            echo "ERROR TRYING TO UPDATE LAGERBESTAND";
            return;
        }


    }
    echo "SUCCESS ADDING VERKAUF. GESAMTPREIS: " . $gesamtpreis;
    return;

}

function sendPreisliste($data,$db){
   echo json_encode(getPreisliste($data,$db));
   return;
}

function getPreisliste($data,$db){
    $sql='SELECT * FROM produkt;';
    $result = $db->query($sql);
    for($i=1;$i<9;$i++){
        $json[$i] = $result->fetch_assoc();
    }
    $json = utf8ize($json);
    return $json;
}

function sendLagerbestand($data, $db){
    $vkortnr = $data['vkortnr'];
    for($i=1;$i<9;$i++){
        $sql='SELECT * FROM lagerbestand WHERE vkortnr= ' . $vkortnr . ' AND artikelnr=' . $i . ';';
        $result = $db->query($sql);
        if(empty($result)){
            echo "ERROR GETTING LAGERBESTAND " . $i;
            return;
        }
        $anzahl[$i] = $result->fetch_assoc();
    }
    echo json_encode(utf8ize($anzahl));
    return;

}









function utf8ize($mixed) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else if (is_string ($mixed)) {
        return utf8_encode($mixed);
    }
    return $mixed;
}