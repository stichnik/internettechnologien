const products = ["milch", "emmentaler", "gouda", "joghurt100", "quark", "joghurt500", "streichkaese", "bergkaese"];



function lieferungBuchen(){
    let url = "beta.php";
    const method = "action=POST&type=lief";


    if(document.getElementById("vkortnr")<1)
        return;
    let vkortnr = document.getElementById("vkortnr").value;
    url += "?"+method+"&"+"vkortnr"+"="+vkortnr;
    let data="";
    for(let i=0; i<8; i++){
        if(i!==0)data+="&";
        let input = document.getElementById("input-"+(i+1).toString());
        if(input<1||input===null){
            data += products[i]+"="+"0";
        }else{
            data += products[i]+"="+input.value;
        }
    }
    console.log(url);
    console.log(data);
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST",url);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.onload = function(){
        alert(xhttp.responseText);
    };
    xhttp.send(data);
}

function verkaufBuchen(){
    let url = "beta.php";
    const method = "action=POST&type=verk";

    if(document.getElementById("vkortnr")<1||document.getElementById("vkortnr").value==="") {
        alert("Bitte geben Sie zuerst eine vkortnr ein.");
        return;
    }
    let vkortnr = document.getElementById("vkortnr").value;
    url += "?"+method+"&"+"vkortnr"+"="+vkortnr;
    let data="";
    for(let i=0;i<8;i++){
        if(i!==0)data+="&";
        let inputs = document.getElementById("input-vk-"+(i+1).toString());
        if(inputs.value<1||inputs.value===""){
            data+=products[i]+"="+"0";
        }else{
            data+=products[i]+"="+inputs.value;
        }
    }
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST",url);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhttp.onload = function () {
        alert(xhttp.responseText);
        getLagermengen();
    };
    xhttp.send(data);
}

function getLagermengen(){
    let url = "beta.php";
    const method = "?action=GET&type=inv";
    if(document.getElementById("vkortnr").value<1){
        return;
    }
    let vkortnr = document.getElementById("vkortnr").value;
    url+=method+"&vkortnr="+vkortnr;
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", url);
    xhttp.onload = function () {
        if(xhttp.status===200){
            let lieferliste = xhttp.response;
            lieferliste = JSON.parse(lieferliste);
            for(let i=1;i<9;i++){
                let span = document.getElementById("aktuell-"+i.toString());
                span.innerText = lieferliste[i]['anzahl'];

            }
            if(document.contains(document.getElementById("input-1")))
                setInputDefault();
        }

    };
    xhttp.send(null);
}

function setInputDefault(){
    for(let i=1;i<9;i++) {
        let span = document.getElementById("aktuell-" + i.toString());
        let input = document.getElementById("input-" + i.toString());
        input.value = 50 - span.innerText;
    }
}

function getPreise(){
    let url = "beta.php";
    const method = "?action=GET&type=preis";
    url+=method;
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", url);
    xhttp.onload = function () {
        if(xhttp.status===200){
            let preisliste = xhttp.response;
            preisliste = JSON.parse(preisliste);
            for(let i=1;i<9;i++){
                let span = document.getElementById("preis-"+ i.toString());
                let preis = preisliste[i]['preis'];
                if(preis.toString().includes(".")){
                    if(preis.toString().split(".")[1].length === 0){
                        preis+="00€";
                    }else if(preis.toString().split(".")[1].length === 1){
                        preis+="0€";
                    }else if(preis.toString().split(".")[1].length === 2){
                        preis+="€";
                    }
                }else {
                    preis += ".00€";
                }
                span.innerText = preis;
            }
        }
    };
    xhttp.send(null);
}