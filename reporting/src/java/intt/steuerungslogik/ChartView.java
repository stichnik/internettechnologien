/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.steuerungslogik;

import intt.datenlogik.Lagerbestand;
import intt.datenlogik.Produkt;
import intt.datenlogik.Verkauf;
import intt.datenlogik.Verkaufprodukt;
import intt.datenlogik.Verkaufsort;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import javax.faces.bean.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Niklas
 */
@ManagedBean
@SessionScoped
public class ChartView implements Serializable {
    private PieChartModel pieModel;
    private BarChartModel barModel;
    private PieChartModel produktModel;
    private PieChartModel stellenModel;
    private BarChartModel zeitModel;    
    private DateHelper[] dates;
    private int chosenYear, chosenMonth;
    private Vector days;
    private boolean monthView;
    private boolean dayView;
    @EJB
    private intt.geschaeftslogik.ProduktFacade produktFacade;
    @EJB
    private intt.geschaeftslogik.LagerbestandFacade lagerbestandFacade;
    @EJB
    private intt.geschaeftslogik.VerkaufproduktFacade verkaufproduktFacade;
    @EJB
    private intt.geschaeftslogik.VerkaufFacade verkaufFacade;
    @EJB
    private intt.geschaeftslogik.VerkaufsortFacade verkaufsortFacade;
    
    @PostConstruct
    public void init(){
        createPieModels();
        createBarModels();
        days=new Vector();
        chosenYear=0;
        chosenMonth=0;
        monthView=false;
        dayView=false;
    }
    //Diagramme erstellen
    private void createPieModels(){
        //Pro Produkt
        createProduktModel();
        //Pro Verkaufsstellen
        createStellenModel();
        //Für Cockpit
        createPieModel();
    }
    
    private void createBarModels(){
        //Pro Monat
        
        createZeitModel();
        createBarModel();
    }
    
    public PieChartModel getPieModel(){
        return pieModel;
    }
    
    public BarChartModel getBarModel(){
        return barModel;
    }
    public PieChartModel getProduktModel(){
        return produktModel;
    }
    public PieChartModel getStellenModel(){
        return stellenModel;
    }
    public BarChartModel getZeitModel(){
        return zeitModel;
    }
    
    //Anteil eines Produktes am Gesamtlagerbestand
    private void createPieModel(){
        pieModel = new PieChartModel();
        List<Produkt> produktList = produktFacade.findAll();
        Iterator<Produkt> iterator = produktList.iterator();
        while(iterator.hasNext()){
            Produkt prod = (Produkt) iterator.next();
            pieModel.set(prod.getName(), getSelectedProduktAmount(prod));
        }
        pieModel.setTitle("Anteile Produkte am Gesamtlagerbestand");
        pieModel.setLegendPosition("w");
        pieModel.setFill(true);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(150);
    }
    
    private void createBarModel(){
        barModel = initBarModel();
        barModel.setTitle("Produktinformationen");
        barModel.setLegendPosition("n");
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Produkte");
        
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Preise / Kosten");
        yAxis.setMin(0);
        yAxis.setMax(4);
    }
    
    private BarChartModel initBarModel(){
        BarChartModel model = new BarChartModel();  
        ChartSeries price = new ChartSeries();
        price.setLabel("Preis");
        ChartSeries cost = new ChartSeries();
        cost.setLabel("Herstellkosten");
        List<Produkt> inventoryList = produktFacade.findAll();
        Iterator<Produkt> iterator = inventoryList.iterator();
        while(iterator.hasNext()){
            Produkt prod = iterator.next();
            price.set(prod.getName(), prod.getPreis());
            
            //Zufallsgenerator für Produktionskosten
            String randomnumber = "";
            Random randomgenerator = new Random();
            int zahl = randomgenerator.nextInt(2);
            randomnumber+=zahl;
            zahl = randomgenerator.nextInt(99);
            randomnumber+="."+zahl;

            cost.set(prod.getName(), Float.parseFloat(randomnumber));
        }
        model.addSeries(price);
        model.addSeries(cost);
        return model;
    }
    
    //Selector
    public void itemSelect (ItemSelectEvent event){
        
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Etwas ist schiefgelaufen",
                                  "ERROR: not initialized");
        if(monthView==false){
            //EINZELNER MONAT
            monthView = true;
            chosenMonth = dates[event.getItemIndex()].month;
            chosenYear = dates[event.getItemIndex()].year;
            //Wir holen uns den Monat und das Jahr aus dem Array
            //Damit können wir eine neue Query erstellen, um alle Umsatztage des Monats aufzuzählen
            float highest=0;
            boolean empty = true;
            ChartSeries umsatz = new ChartSeries();
            umsatz.setLabel("Monate");
            int pos=0;
            days = new Vector();
            for(int i=1;i<32;i++){
                List<Verkauf> sales = verkaufFacade.verkaufByDayMonthYear(i, chosenMonth, chosenYear);
                //Wir fügen den Tag nur hinzu, falls es auch Umsätze gab
                if(!sales.isEmpty()){
                    empty = false;
                    Iterator it = sales.iterator();
                    float sum=0;
                    while(it.hasNext()){
                        Verkauf sale = (Verkauf)it.next();
                        sum+=sale.getWarenwert();
                    }
                    umsatz.set(i+"/"+chosenMonth+"/"+chosenYear, sum);
                    if(sum>highest)
                        highest=sum;
                    //Tag in die Liste hinzufügen
                    days.add(i);
                }
            }
            if(empty){
                //Sollte theoretisch nie erreicht werden
                createZeitModel();
                msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Keine Umsätze gefunden",
                                  "Für diesen Monat gibt es keine Umsätze.");
            }else{
                zeitModel = new BarChartModel();
                umsatz.setLabel("Tage");
                zeitModel.addSeries(umsatz);
                zeitModel.setTitle("Umsatzinformation");
                zeitModel.setLegendPosition("n");
                Axis xAxis = zeitModel.getAxis(AxisType.X);
                xAxis.setLabel("Monat & Jahr");
                Axis yAxis = zeitModel.getAxis(AxisType.Y);
                yAxis.setLabel("Umsatz");
                yAxis.setMin(0);
                yAxis.setMax(highest);
                msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Auswahl",
                                "Ansicht für Monat "+dates[event.getItemIndex()].month+
                                ", Jahr "+dates[event.getItemIndex()].year+" geladen");
            }
        }else{
            if(dayView==false){
                dayView=true;
                //EINZELNER TAG
                //Zunächst müssen wir herausfinden welcher Tag, Monat und Jahr nun ausgewählt sind
                ChartSeries umsatz = new ChartSeries();
                umsatz.setLabel("Produkte");
                int day=(Integer)days.get(event.getItemIndex());
                List<Verkauf> sales = verkaufFacade.verkaufByDayMonthYear(day, chosenMonth, chosenYear);
                Iterator it = sales.iterator();
                double highest=0;
                double[] values = new double[8];
                while(it.hasNext()){
                    Verkauf sale = (Verkauf)it.next();
                    Collection<Verkaufprodukt> vkprods = sale.getVerkaufproduktCollection();
                    Iterator it2 = vkprods.iterator();
                    while(it2.hasNext()){
                        Verkaufprodukt vkprod = (Verkaufprodukt) it2.next();
                        Produkt prod = (Produkt) vkprod.getArtikelnr();
                        values[prod.getArtikelnr()-1] = vkprod.getAnzahl()*prod.getPreis();
                    }
                }
                List<Produkt> products = produktFacade.findAll();
                it = products.iterator();
                while(it.hasNext()){
                    Produkt product = (Produkt) it.next();
                    umsatz.set(product.getName(), values[product.getArtikelnr()-1]);
                    if(values[product.getArtikelnr()-1]>highest)
                        highest=values[product.getArtikelnr()-1];
                }
                zeitModel = new BarChartModel();
                zeitModel.addSeries(umsatz);
                zeitModel.setTitle("Umsatzinformation");
                zeitModel.setLegendPosition("n");
                Axis xAxis = zeitModel.getAxis(AxisType.X);
                xAxis.setLabel("Produkte");
                Axis yAxis = zeitModel.getAxis(AxisType.Y);
                yAxis.setLabel("Umsatz");
                yAxis.setMin(0);
                yAxis.setMax(highest);
                msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Auswahl",
                                "Detailansicht für "+day+"."+chosenMonth+"."+chosenYear);
            }else{
                //24 MONATE ANSICHT
                dayView=false;
                monthView=false;
                days.clear();
                createZeitModel();
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public Integer getSelectedProduktAmount(intt.datenlogik.Produkt produkt){
        List<Lagerbestand> lagerlist = lagerbestandFacade.lagerbestandByProdukt(produkt);
        int amount = 0;
        Iterator<Lagerbestand> iterator = lagerlist.iterator();
        while(iterator.hasNext()){
            Lagerbestand lagerbestand = (Lagerbestand) iterator.next();
            amount+=lagerbestand.getAnzahl();
        }
        return amount;
    }

    private void createProduktModel() {
        produktModel = new PieChartModel();
        List<Produkt> products = produktFacade.findAll();
        Iterator it = products.iterator();
        while(it.hasNext()){
            float sum=0;
            Produkt prod = (Produkt) it.next();
            List<Verkaufprodukt> vkproducts = verkaufproduktFacade.verkaufprodByProdukt(prod);
            for (Verkaufprodukt vkprod : vkproducts) {
                sum+=vkprod.getAnzahl()*prod.getPreis();
            }
            produktModel.set(prod.getName(),sum);
        }
        produktModel.setTitle("Anteile Produkte am Gesamtumsatz");
        produktModel.setLegendPosition("w");
        produktModel.setFill(true);
        produktModel.setShowDataLabels(true);
        produktModel.setDiameter(150);
    }

    private void createStellenModel() {
        stellenModel = new PieChartModel();
        List<Verkaufsort> verkaufsorte = verkaufsortFacade.findAll();
        Iterator it = verkaufsorte.iterator();
        while(it.hasNext()){
            float sum=0;
            Verkaufsort vkort = (Verkaufsort) it.next();
            List<Verkauf> sales = verkaufFacade.verkaufByOrt(vkort);
            for(Verkauf sale : sales) {
                sum+=sale.getWarenwert();
            }
            stellenModel.set(vkort.getBeschreibung()+" "+vkort.getAdresse(),sum);
        }
        stellenModel.setTitle("Anteile Verkaufsort an Gesamtumsatz");
        stellenModel.setLegendPosition("w");
        stellenModel.setFill(true);
        stellenModel.setShowDataLabels(true);
        stellenModel.setDiameter(150);
    }

    private void createZeitModel() {
        zeitModel = new BarChartModel();
        //Letzte 24 Monate anzeigen
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        ChartSeries umsatz = new ChartSeries();
        float highest=0;
        dates = new DateHelper[24];
        for(int i=0;i<24;i++){
            List<Verkauf> sales = verkaufFacade.verkaufByMonthYear(year, month);
            Iterator it = sales.iterator();
            float sum=0;
            while(it.hasNext()){
                Verkauf verk = (Verkauf) it.next();
                sum += verk.getWarenwert();
            }
            dates[i] = new DateHelper(month, year);
            umsatz.set(month+"/"+(year-2000), sum);
            if(highest<sum)
                highest=sum;
            month--;
            if(month==0){
                month=12;
                year--;
            }
        }
        zeitModel.addSeries(umsatz);
        zeitModel.setTitle("Umsatzinformation");
        zeitModel.setLegendPosition("n");
        Axis xAxis = zeitModel.getAxis(AxisType.X);
        xAxis.setLabel("Monat & Jahr");
        Axis yAxis = zeitModel.getAxis(AxisType.Y);
        yAxis.setLabel("Umsatz");
        yAxis.setMin(0);
        yAxis.setMax(highest);
    }

    
}
