package intt.steuerungslogik;

import intt.datenlogik.Lagerbestand;
import intt.steuerungslogik.util.JsfUtil;
import intt.steuerungslogik.util.PaginationHelper;
import intt.geschaeftslogik.LagerbestandFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.List;
import java.util.Iterator;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("lagerbestandController")
@SessionScoped
public class LagerbestandController implements Serializable {

    private Lagerbestand current;
    private DataModel items = null;
    @EJB
    private intt.geschaeftslogik.LagerbestandFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public LagerbestandController() {
    }

    public Lagerbestand getSelected() {
        if (current == null) {
            current = new Lagerbestand();
            selectedItemIndex = -1;
        }
        return current;
    }

    private LagerbestandFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()-1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Lagerbestand) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Lagerbestand();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Bundle").getString("LagerbestandCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Lagerbestand) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Bundle").getString("LagerbestandUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Lagerbestand) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Bundle").getString("LagerbestandDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Lagerbestand getLagerbestand(java.lang.Integer id) {
        return ejbFacade.find(id);
    }
    
    // Neue Methode für Lagerbestand nach Station
    public String prepareLagerbestandByVerkaufsort(){
        current = (Lagerbestand) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        //Rückgabe der nächsten View die aufgerufen werden soll
        return "Stationsbestand";
    }
    
    public List<Lagerbestand> getLagerbestandByVerkaufsort(intt.datenlogik.Verkaufsort verkaufsort){
        return ejbFacade.lagerbestandByVerkaufsort(verkaufsort);
    }

    public List<Lagerbestand> getLagerbestandByProduktOrder(intt.datenlogik.Produkt artikelnr){
        return ejbFacade.getItemOrderBy("artikelnr", artikelnr.getArtikelnr(), "vkortnr");
    }
    
    public String prepareLagerbestandByProdukt(){
        current = (Lagerbestand) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        //Rückgabe der nächsten View die aufgerufen werden soll
        return "Produktbestand";
    }
    //Gesamtbestand eines Produktes
    public String prepareLagerbestandByOneProduktAmount(){
        current = (Lagerbestand) getItems().getRowData();
        //Rückgabe der nächsten View die aufgerufen werden soll
        return "GesamtlagereinProdukt";
    }
    
    public Integer getSelectedProduktAmount(intt.datenlogik.Produkt produkt){
        List<Lagerbestand> lagerlist = ejbFacade.lagerbestandByProdukt(produkt);
        int amount = 0;
        Iterator<Lagerbestand> iterator = lagerlist.iterator();
        while(iterator.hasNext()){
            Lagerbestand lagerbestand = (Lagerbestand) iterator.next();
            amount+=lagerbestand.getAnzahl();
        }
        return amount;
    }
    //Gesamtbestände aller Produkte
    public String prepareLagerbestandByAllProduktAmount(){
        //Rückgabe der nächsten View die aufgerufen werden soll
        return "GesamtlageralleProdukte";
    }
    
    @FacesConverter(forClass = Lagerbestand.class)
    public static class LagerbestandControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LagerbestandController controller = (LagerbestandController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "lagerbestandController");
            return controller.getLagerbestand(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Lagerbestand) {
                Lagerbestand o = (Lagerbestand) object;
                return getStringKey(o.getInventarnr());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Lagerbestand.class.getName());
            }
        }

    }

}
