/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "lieferung")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lieferung.findAll", query = "SELECT l FROM Lieferung l")
    , @NamedQuery(name = "Lieferung.findByLiefernr", query = "SELECT l FROM Lieferung l WHERE l.liefernr = :liefernr")
    , @NamedQuery(name = "Lieferung.findByDatum", query = "SELECT l FROM Lieferung l WHERE l.datum = :datum")})
public class Lieferung implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "liefernr")
    private Integer liefernr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datum")
    @Temporal(TemporalType.DATE)
    private Date datum;
    @JoinColumn(name = "vkortnr", referencedColumnName = "vkortnr")
    @ManyToOne(optional = false)
    private Verkaufsort vkortnr;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liefernr")
    private Collection<Lieferungprodukt> lieferungproduktCollection;

    public Lieferung() {
    }

    public Lieferung(Integer liefernr) {
        this.liefernr = liefernr;
    }

    public Lieferung(Integer liefernr, Date datum) {
        this.liefernr = liefernr;
        this.datum = datum;
    }

    public Integer getLiefernr() {
        return liefernr;
    }

    public void setLiefernr(Integer liefernr) {
        this.liefernr = liefernr;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Verkaufsort getVkortnr() {
        return vkortnr;
    }

    public void setVkortnr(Verkaufsort vkortnr) {
        this.vkortnr = vkortnr;
    }

    @XmlTransient
    public Collection<Lieferungprodukt> getLieferungproduktCollection() {
        return lieferungproduktCollection;
    }

    public void setLieferungproduktCollection(Collection<Lieferungprodukt> lieferungproduktCollection) {
        this.lieferungproduktCollection = lieferungproduktCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (liefernr != null ? liefernr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lieferung)) {
            return false;
        }
        Lieferung other = (Lieferung) object;
        if ((this.liefernr == null && other.liefernr != null) || (this.liefernr != null && !this.liefernr.equals(other.liefernr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Lieferung[ liefernr=" + liefernr + " ]";
    }
    
}
