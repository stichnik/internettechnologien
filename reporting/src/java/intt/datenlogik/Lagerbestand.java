/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "lagerbestand")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lagerbestand.findAll", query = "SELECT l FROM Lagerbestand l")
    , @NamedQuery(name= "Lagerbestand.findByVkortnr", query = "SELECT l FROM Lagerbestand l WHERE l.vkortnr = :vkortnr")
    , @NamedQuery(name= "Lagerbestand.findByArtikelnr", query = "SELECT l FROM Lagerbestand l WHERE l.artikelnr = :artikelnr")
    , @NamedQuery(name = "Lagerbestand.findByInventarnr", query = "SELECT l FROM Lagerbestand l WHERE l.inventarnr = :inventarnr")
    , @NamedQuery(name = "Lagerbestand.findByAnzahl", query = "SELECT l FROM Lagerbestand l WHERE l.anzahl = :anzahl")})
public class Lagerbestand implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "inventarnr")
    private Integer inventarnr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anzahl")
    private int anzahl;
    @JoinColumn(name = "artikelnr", referencedColumnName = "artikelnr")
    @ManyToOne(optional = false)
    private Produkt artikelnr;
    @JoinColumn(name = "vkortnr", referencedColumnName = "vkortnr")
    @ManyToOne(optional = false)
    private Verkaufsort vkortnr;

    public Lagerbestand() {
    }

    public Lagerbestand(Integer inventarnr) {
        this.inventarnr = inventarnr;
    }

    public Lagerbestand(Integer inventarnr, int anzahl) {
        this.inventarnr = inventarnr;
        this.anzahl = anzahl;
    }

    public Integer getInventarnr() {
        return inventarnr;
    }

    public void setInventarnr(Integer inventarnr) {
        this.inventarnr = inventarnr;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public Produkt getArtikelnr() {
        return artikelnr;
    }

    public void setArtikelnr(Produkt artikelnr) {
        this.artikelnr = artikelnr;
    }

    public Verkaufsort getVkortnr() {
        return vkortnr;
    }

    public void setVkortnr(Verkaufsort vkortnr) {
        this.vkortnr = vkortnr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inventarnr != null ? inventarnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lagerbestand)) {
            return false;
        }
        Lagerbestand other = (Lagerbestand) object;
        if ((this.inventarnr == null && other.inventarnr != null) || (this.inventarnr != null && !this.inventarnr.equals(other.inventarnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Lagerbestand[ inventarnr=" + inventarnr + " ]";
    }
    
}
