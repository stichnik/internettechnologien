/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "lieferungprodukt")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lieferungprodukt.findAll", query = "SELECT l FROM Lieferungprodukt l")
    , @NamedQuery(name = "Lieferungprodukt.findByLiefprodnr", query = "SELECT l FROM Lieferungprodukt l WHERE l.liefprodnr = :liefprodnr")
    , @NamedQuery(name = "Lieferungprodukt.findByAnzahl", query = "SELECT l FROM Lieferungprodukt l WHERE l.anzahl = :anzahl")})
public class Lieferungprodukt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "liefprodnr")
    private Integer liefprodnr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anzahl")
    private int anzahl;
    @JoinColumn(name = "artikelnr", referencedColumnName = "artikelnr")
    @ManyToOne(optional = false)
    private Produkt artikelnr;
    @JoinColumn(name = "liefernr", referencedColumnName = "liefernr")
    @ManyToOne(optional = false)
    private Lieferung liefernr;

    public Lieferungprodukt() {
    }

    public Lieferungprodukt(Integer liefprodnr) {
        this.liefprodnr = liefprodnr;
    }

    public Lieferungprodukt(Integer liefprodnr, int anzahl) {
        this.liefprodnr = liefprodnr;
        this.anzahl = anzahl;
    }

    public Integer getLiefprodnr() {
        return liefprodnr;
    }

    public void setLiefprodnr(Integer liefprodnr) {
        this.liefprodnr = liefprodnr;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public Produkt getArtikelnr() {
        return artikelnr;
    }

    public void setArtikelnr(Produkt artikelnr) {
        this.artikelnr = artikelnr;
    }

    public Lieferung getLiefernr() {
        return liefernr;
    }

    public void setLiefernr(Lieferung liefernr) {
        this.liefernr = liefernr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (liefprodnr != null ? liefprodnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lieferungprodukt)) {
            return false;
        }
        Lieferungprodukt other = (Lieferungprodukt) object;
        if ((this.liefprodnr == null && other.liefprodnr != null) || (this.liefprodnr != null && !this.liefprodnr.equals(other.liefprodnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Lieferungprodukt[ liefprodnr=" + liefprodnr + " ]";
    }
    
}
