/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "verkauf")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verkauf.findAll", query = "SELECT v FROM Verkauf v")
    , @NamedQuery(name = "Verkauf.findByVerkfnr", query = "SELECT v FROM Verkauf v WHERE v.verkfnr = :verkfnr")
    , @NamedQuery(name = "Verkauf.findByDatum", query = "SELECT v FROM Verkauf v WHERE v.datum = :datum")
    , @NamedQuery(name = "Verkauf.findByVkortnr", query = "SELECT v FROM Verkauf v WHERE v.vkortnr = :vkortnr")
    , @NamedQuery(name = "Verkauf.findByWarenwert", query = "SELECT v FROM Verkauf v WHERE v.warenwert = :warenwert")})
@NamedNativeQueries({
    @NamedNativeQuery(name = "Verkauf.findByYear", query = "SELECT * FROM Verkauf v WHERE YEAR(v.datum) = ?", resultClass=intt.datenlogik.Verkauf.class)
    , @NamedNativeQuery(name = "Verkauf.findByMonth", query = "SELECT * FROM Verkauf v WHERE MONTH(v.datum) = ?", resultClass=intt.datenlogik.Verkauf.class)
    , @NamedNativeQuery(name = "Verkauf.findByDay", query = "SELECT * FROM Verkauf v WHERE DAY(v.datum) = ?", resultClass=intt.datenlogik.Verkauf.class)
    , @NamedNativeQuery(name = "Verkauf.findByYearMonth", query = "SELECT * FROM Verkauf v WHERE YEAR(v.datum) = ? AND MONTH(v.datum) = ?", resultClass=intt.datenlogik.Verkauf.class)
    , @NamedNativeQuery(name = "Verkauf.findByYearMonthDay", query = "SELECT * FROM Verkauf v WHERE YEAR(v.datum) = ? AND MONTH(v.datum) = ? AND DAY(v.datum) = ? ", resultClass=intt.datenlogik.Verkauf.class)
})
public class Verkauf implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "verkfnr")
    private Integer verkfnr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datum")
    @Temporal(TemporalType.DATE)
    private Date datum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "warenwert")
    private double warenwert;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "verkfnr")
    private Collection<Verkaufprodukt> verkaufproduktCollection;
    @JoinColumn(name = "verkaeufernr", referencedColumnName = "persnr")
    @ManyToOne
    private Verkaeufer verkaeufernr;
    @JoinColumn(name = "vkortnr", referencedColumnName = "vkortnr")
    @ManyToOne(optional = false)
    private Verkaufsort vkortnr;

    public Verkauf() {
    }

    public Verkauf(Integer verkfnr) {
        this.verkfnr = verkfnr;
    }

    public Verkauf(Integer verkfnr, Date datum, double warenwert) {
        this.verkfnr = verkfnr;
        this.datum = datum;
        this.warenwert = warenwert;
    }

    public Integer getVerkfnr() {
        return verkfnr;
    }

    public void setVerkfnr(Integer verkfnr) {
        this.verkfnr = verkfnr;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public double getWarenwert() {
        return warenwert;
    }

    public void setWarenwert(double warenwert) {
        this.warenwert = warenwert;
    }

    @XmlTransient
    public Collection<Verkaufprodukt> getVerkaufproduktCollection() {
        return verkaufproduktCollection;
    }

    public void setVerkaufproduktCollection(Collection<Verkaufprodukt> verkaufproduktCollection) {
        this.verkaufproduktCollection = verkaufproduktCollection;
    }

    public Verkaeufer getVerkaeufernr() {
        return verkaeufernr;
    }

    public void setVerkaeufernr(Verkaeufer verkaeufernr) {
        this.verkaeufernr = verkaeufernr;
    }

    public Verkaufsort getVkortnr() {
        return vkortnr;
    }

    public void setVkortnr(Verkaufsort vkortnr) {
        this.vkortnr = vkortnr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (verkfnr != null ? verkfnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verkauf)) {
            return false;
        }
        Verkauf other = (Verkauf) object;
        if ((this.verkfnr == null && other.verkfnr != null) || (this.verkfnr != null && !this.verkfnr.equals(other.verkfnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Verkauf[ verkfnr=" + verkfnr + " ]";
    }
    
}
