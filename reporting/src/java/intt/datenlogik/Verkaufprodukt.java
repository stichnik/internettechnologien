/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "verkaufprodukt")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verkaufprodukt.findAll", query = "SELECT v FROM Verkaufprodukt v")
    , @NamedQuery(name = "Verkaufprodukt.findByVerkfprodnr", query = "SELECT v FROM Verkaufprodukt v WHERE v.verkfprodnr = :verkfprodnr")
    , @NamedQuery(name = "Verkaufprodukt.findByArtikelnr", query = "SELECT v FROM Verkaufprodukt v WHERE v.artikelnr = :artikelnr")
    , @NamedQuery(name = "Verkaufprodukt.findByAnzahl", query = "SELECT v FROM Verkaufprodukt v WHERE v.anzahl = :anzahl")
    , @NamedQuery(name = "Verkaufprodukt.findByVerkfnrProd", query = "SELECT v FROM Verkaufprodukt v WHERE v.verkfnr = :verkfnr AND v.artikelnr = :prodnr")})
    
public class Verkaufprodukt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "verkfprodnr")
    private Integer verkfprodnr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anzahl")
    private int anzahl;
    @JoinColumn(name = "artikelnr", referencedColumnName = "artikelnr")
    @ManyToOne(optional = false)
    private Produkt artikelnr;
    @JoinColumn(name = "verkfnr", referencedColumnName = "verkfnr")
    @ManyToOne(optional = false)
    private Verkauf verkfnr;

    public Verkaufprodukt() {
    }

    public Verkaufprodukt(Integer verkfprodnr) {
        this.verkfprodnr = verkfprodnr;
    }

    public Verkaufprodukt(Integer verkfprodnr, int anzahl) {
        this.verkfprodnr = verkfprodnr;
        this.anzahl = anzahl;
    }

    public Integer getVerkfprodnr() {
        return verkfprodnr;
    }

    public void setVerkfprodnr(Integer verkfprodnr) {
        this.verkfprodnr = verkfprodnr;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public Produkt getArtikelnr() {
        return artikelnr;
    }

    public void setArtikelnr(Produkt artikelnr) {
        this.artikelnr = artikelnr;
    }

    public Verkauf getVerkfnr() {
        return verkfnr;
    }

    public void setVerkfnr(Verkauf verkfnr) {
        this.verkfnr = verkfnr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (verkfprodnr != null ? verkfprodnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verkaufprodukt)) {
            return false;
        }
        Verkaufprodukt other = (Verkaufprodukt) object;
        if ((this.verkfprodnr == null && other.verkfprodnr != null) || (this.verkfprodnr != null && !this.verkfprodnr.equals(other.verkfprodnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Verkaufprodukt[ verkfprodnr=" + verkfprodnr + " ]";
    }
    
}
