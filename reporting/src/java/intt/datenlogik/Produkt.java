/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "produkt")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produkt.findAll", query = "SELECT p FROM Produkt p")
    , @NamedQuery(name = "Produkt.findByArtikelnr", query = "SELECT p FROM Produkt p WHERE p.artikelnr = :artikelnr")
    , @NamedQuery(name = "Produkt.findByName", query = "SELECT p FROM Produkt p WHERE p.name = :name")
    , @NamedQuery(name = "Produkt.findByPreis", query = "SELECT p FROM Produkt p WHERE p.preis = :preis")
    , @NamedQuery(name = "Produkt.findByHaltbarkeit", query = "SELECT p FROM Produkt p WHERE p.haltbarkeit = :haltbarkeit")})
public class Produkt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "artikelnr")
    private Integer artikelnr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preis")
    private double preis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "haltbarkeit")
    private int haltbarkeit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artikelnr")
    private Collection<Verkaufprodukt> verkaufproduktCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artikelnr")
    private Collection<Lagerbestand> lagerbestandCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artikelnr")
    private Collection<Lieferungprodukt> lieferungproduktCollection;

    public Produkt() {
    }

    public Produkt(Integer artikelnr) {
        this.artikelnr = artikelnr;
    }

    public Produkt(Integer artikelnr, String name, double preis, int haltbarkeit) {
        this.artikelnr = artikelnr;
        this.name = name;
        this.preis = preis;
        this.haltbarkeit = haltbarkeit;
    }

    public Integer getArtikelnr() {
        return artikelnr;
    }

    public void setArtikelnr(Integer artikelnr) {
        this.artikelnr = artikelnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public int getHaltbarkeit() {
        return haltbarkeit;
    }

    public void setHaltbarkeit(int haltbarkeit) {
        this.haltbarkeit = haltbarkeit;
    }

    @XmlTransient
    public Collection<Verkaufprodukt> getVerkaufproduktCollection() {
        return verkaufproduktCollection;
    }

    public void setVerkaufproduktCollection(Collection<Verkaufprodukt> verkaufproduktCollection) {
        this.verkaufproduktCollection = verkaufproduktCollection;
    }

    @XmlTransient
    public Collection<Lagerbestand> getLagerbestandCollection() {
        return lagerbestandCollection;
    }

    public void setLagerbestandCollection(Collection<Lagerbestand> lagerbestandCollection) {
        this.lagerbestandCollection = lagerbestandCollection;
    }

    @XmlTransient
    public Collection<Lieferungprodukt> getLieferungproduktCollection() {
        return lieferungproduktCollection;
    }

    public void setLieferungproduktCollection(Collection<Lieferungprodukt> lieferungproduktCollection) {
        this.lieferungproduktCollection = lieferungproduktCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (artikelnr != null ? artikelnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produkt)) {
            return false;
        }
        Produkt other = (Produkt) object;
        if ((this.artikelnr == null && other.artikelnr != null) || (this.artikelnr != null && !this.artikelnr.equals(other.artikelnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Produkt[ artikelnr=" + artikelnr + " ]";
    }
    
}
