/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "verkaufsort")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verkaufsort.findAll", query = "SELECT v FROM Verkaufsort v")
    , @NamedQuery(name = "Verkaufsort.findByVkortnr", query = "SELECT v FROM Verkaufsort v WHERE v.vkortnr = :vkortnr")
    , @NamedQuery(name = "Verkaufsort.findByTyp", query = "SELECT v FROM Verkaufsort v WHERE v.typ = :typ")})
public class Verkaufsort implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vkortnr")
    private Integer vkortnr;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "coordA")
    private String coordA;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "coordL")
    private String coordL;
    @Lob
    @Size(max = 65535)
    @Column(name = "adresse")
    private String adresse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "typ")
    private String typ;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "beschreibung")
    private String beschreibung;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vkortnr")
    private Collection<Lieferung> lieferungCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vkortnr")
    private Collection<Lagerbestand> lagerbestandCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vkortnr")
    private Collection<Verkauf> verkaufCollection;

    public Verkaufsort() {
    }

    public Verkaufsort(Integer vkortnr) {
        this.vkortnr = vkortnr;
    }

    public Verkaufsort(Integer vkortnr, String coordA, String coordL, String typ, String beschreibung) {
        this.vkortnr = vkortnr;
        this.coordA = coordA;
        this.coordL = coordL;
        this.typ = typ;
        this.beschreibung = beschreibung;
    }

    public Integer getVkortnr() {
        return vkortnr;
    }

    public void setVkortnr(Integer vkortnr) {
        this.vkortnr = vkortnr;
    }

    public String getCoordA() {
        return coordA;
    }

    public void setCoordA(String coordA) {
        this.coordA = coordA;
    }

    public String getCoordL() {
        return coordL;
    }

    public void setCoordL(String coordL) {
        this.coordL = coordL;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    @XmlTransient
    public Collection<Lieferung> getLieferungCollection() {
        return lieferungCollection;
    }

    public void setLieferungCollection(Collection<Lieferung> lieferungCollection) {
        this.lieferungCollection = lieferungCollection;
    }

    @XmlTransient
    public Collection<Lagerbestand> getLagerbestandCollection() {
        return lagerbestandCollection;
    }

    public void setLagerbestandCollection(Collection<Lagerbestand> lagerbestandCollection) {
        this.lagerbestandCollection = lagerbestandCollection;
    }

    @XmlTransient
    public Collection<Verkauf> getVerkaufCollection() {
        return verkaufCollection;
    }

    public void setVerkaufCollection(Collection<Verkauf> verkaufCollection) {
        this.verkaufCollection = verkaufCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vkortnr != null ? vkortnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verkaufsort)) {
            return false;
        }
        Verkaufsort other = (Verkaufsort) object;
        if ((this.vkortnr == null && other.vkortnr != null) || (this.vkortnr != null && !this.vkortnr.equals(other.vkortnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Verkaufsort[ vkortnr=" + vkortnr + " ]";
    }
    
}
