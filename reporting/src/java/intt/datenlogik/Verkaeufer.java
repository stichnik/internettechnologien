/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.datenlogik;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Niklas
 */
@Entity
@Table(name = "verkaeufer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verkaeufer.findAll", query = "SELECT v FROM Verkaeufer v")
    , @NamedQuery(name = "Verkaeufer.findByPersnr", query = "SELECT v FROM Verkaeufer v WHERE v.persnr = :persnr")
    , @NamedQuery(name = "Verkaeufer.findByGeschlecht", query = "SELECT v FROM Verkaeufer v WHERE v.geschlecht = :geschlecht")})
public class Verkaeufer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "persnr")
    private Integer persnr;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "vorname")
    private String vorname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "geschlecht")
    private String geschlecht;
    @OneToMany(mappedBy = "verkaeufernr")
    private Collection<Verkauf> verkaufCollection;

    public Verkaeufer() {
    }

    public Verkaeufer(Integer persnr) {
        this.persnr = persnr;
    }

    public Verkaeufer(Integer persnr, String name, String vorname, String geschlecht) {
        this.persnr = persnr;
        this.name = name;
        this.vorname = vorname;
        this.geschlecht = geschlecht;
    }

    public Integer getPersnr() {
        return persnr;
    }

    public void setPersnr(Integer persnr) {
        this.persnr = persnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    @XmlTransient
    public Collection<Verkauf> getVerkaufCollection() {
        return verkaufCollection;
    }

    public void setVerkaufCollection(Collection<Verkauf> verkaufCollection) {
        this.verkaufCollection = verkaufCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (persnr != null ? persnr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verkaeufer)) {
            return false;
        }
        Verkaeufer other = (Verkaeufer) object;
        if ((this.persnr == null && other.persnr != null) || (this.persnr != null && !this.persnr.equals(other.persnr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "intt.datenlogik.Verkaeufer[ persnr=" + persnr + " ]";
    }
    
}
