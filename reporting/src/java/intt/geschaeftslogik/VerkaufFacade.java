/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.geschaeftslogik;

import intt.datenlogik.Verkauf;
import intt.datenlogik.Verkaufprodukt;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Niklas
 */
@Stateless
public class VerkaufFacade extends AbstractFacade<Verkauf> {

    @PersistenceContext(unitName = "reportingPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VerkaufFacade() {
        super(Verkauf.class);
    }
    public List<Verkauf> verkaufByOrt(intt.datenlogik.Verkaufsort ort){
        List<Verkauf> verkaufprodukt = em.createNamedQuery("Verkauf.findByVkortnr")
                .setParameter("vkortnr", ort)
                .getResultList();
        return verkaufprodukt;
    }
    
    public List<Verkauf> verkaufByMonthYear(int year, int month){
        List<Verkauf> sales = em.createNamedQuery("Verkauf.findByYearMonth")
                .setParameter(2, month)
                .setParameter(1, year)
                .getResultList();
        return sales;
    }
    
    public List<Verkauf> verkaufByDayMonthYear(int day, int month, int year){
        List<Verkauf> sales = em.createNamedQuery("Verkauf.findByYearMonthDay")
                .setParameter(3, day)
                .setParameter(2, month)
                .setParameter(1, year)
                .getResultList();
        return sales;
    }
}
