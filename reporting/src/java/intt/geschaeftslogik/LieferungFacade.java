/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.geschaeftslogik;

import intt.datenlogik.Lieferung;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Niklas
 */
@Stateless
public class LieferungFacade extends AbstractFacade<Lieferung> {

    @PersistenceContext(unitName = "reportingPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LieferungFacade() {
        super(Lieferung.class);
    }
    
}
