/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.geschaeftslogik;

import intt.datenlogik.Lagerbestand;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * @author Niklas
 */
@Stateless
public class LagerbestandFacade extends AbstractFacade<Lagerbestand> {

    @PersistenceContext(unitName = "reportingPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LagerbestandFacade() {
        super(Lagerbestand.class);
    }
    
    
    public List<Lagerbestand> lagerbestandByVerkaufsort(intt.datenlogik.Verkaufsort verkaufsort){
        List<Lagerbestand> lagerbestand = em.createNamedQuery("Lagerbestand.findByVkortnr")
                .setParameter("vkortnr", verkaufsort)
                .getResultList();
        return lagerbestand;
    }
    public List<Lagerbestand> lagerbestandByProdukt(intt.datenlogik.Produkt produkt){
        List<Lagerbestand> lagerbestand = em.createNamedQuery("Lagerbestand.findByArtikelnr")
                .setParameter("artikelnr", produkt)
                .getResultList();
        return lagerbestand;
    }
    
}
