/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.geschaeftslogik;

import intt.datenlogik.Lieferungprodukt;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Niklas
 */
@Stateless
public class LieferungproduktFacade extends AbstractFacade<Lieferungprodukt> {

    @PersistenceContext(unitName = "reportingPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LieferungproduktFacade() {
        super(Lieferungprodukt.class);
    }
    
}
