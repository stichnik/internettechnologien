/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intt.geschaeftslogik;

import intt.datenlogik.Lagerbestand;
import intt.datenlogik.Verkaufprodukt;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Niklas
 */
@Stateless
public class VerkaufproduktFacade extends AbstractFacade<Verkaufprodukt> {

    @PersistenceContext(unitName = "reportingPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VerkaufproduktFacade() {
        super(Verkaufprodukt.class);
    }
    public List<Verkaufprodukt> verkaufprodByProdukt(intt.datenlogik.Produkt produkt){
        List<Verkaufprodukt> verkaufprodukt = em.createNamedQuery("Verkaufprodukt.findByArtikelnr")
                .setParameter("artikelnr", produkt)
                .getResultList();
        return verkaufprodukt;
    }
    
    public Verkaufprodukt verkaufprodByVerkfnrProdukt(intt.datenlogik.Verkauf verkauf, intt.datenlogik.Produkt produkt){
        return (Verkaufprodukt)em.createNamedQuery("Verkaufprodukt.findByVerkfnrProd")
                .setParameter("verkfnr", verkauf)
                .setParameter("prodnr", produkt)
                .getResultList().get(0);
    }
    
}
